package id.bts.spartacommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bts.spartacommerce.domains.SalesOrderEntity;

public interface SalesOrderRepository extends JpaRepository<SalesOrderEntity, Long> {

}
