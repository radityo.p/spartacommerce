package id.bts.spartacommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bts.spartacommerce.domains.ProductArchiveEntity;

@Repository
public interface ProductArchiveRepository extends JpaRepository<ProductArchiveEntity, Long> {

}
