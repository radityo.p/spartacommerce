package id.bts.spartacommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bts.spartacommerce.domains.AdjustmentEntity;

@Repository
public interface AdjustmentRepository extends JpaRepository<AdjustmentEntity, Long> {

}
