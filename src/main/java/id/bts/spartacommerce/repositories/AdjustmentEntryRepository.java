package id.bts.spartacommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bts.spartacommerce.domains.AdjustmentEntryEntity;

@Repository
public interface AdjustmentEntryRepository extends JpaRepository<AdjustmentEntryEntity, Long> {

	List<AdjustmentEntryEntity> findByAdjustmentEntity_Id(Long id);
}
