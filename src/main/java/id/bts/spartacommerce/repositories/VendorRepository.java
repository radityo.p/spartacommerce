package id.bts.spartacommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bts.spartacommerce.domains.VendorEntity;

@Repository
public interface VendorRepository extends JpaRepository<VendorEntity, Long> {

}
