package id.bts.spartacommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bts.spartacommerce.domains.SalesEntryEntity;

@Repository
public interface SalesEntryRepository extends JpaRepository<SalesEntryEntity, Long> {

	List<SalesEntryEntity> findBySalesOrderEntity_Id(Long id);
}
