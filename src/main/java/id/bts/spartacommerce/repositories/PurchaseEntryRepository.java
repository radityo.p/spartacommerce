package id.bts.spartacommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bts.spartacommerce.domains.PurchaseEntryEntity;

@Repository
public interface PurchaseEntryRepository extends JpaRepository<PurchaseEntryEntity, Long> {

	List<PurchaseEntryEntity> findByPurchaseOrderEntity_Id(Long id);
}
