package id.bts.spartacommerce.domains;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SalesEntryEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Integer quantity;

	@ManyToOne
	@JoinColumn(name = "salesOrder_id", nullable = false)
	private SalesOrderEntity salesOrderEntity;

	@OneToOne
	@JoinColumn(name = "product_id", nullable = false)
	private ProductEntity productEntity;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public SalesOrderEntity getSalesOrderEntity() {
		return salesOrderEntity;
	}

	public void setSalesOrderEntity(SalesOrderEntity salesOrderEntity) {
		this.salesOrderEntity = salesOrderEntity;
	}

	public ProductEntity getProductEntity() {
		return productEntity;
	}

	public void setProductEntity(ProductEntity productEntity) {
		this.productEntity = productEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((productEntity == null) ? 0 : productEntity.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((salesOrderEntity == null) ? 0 : salesOrderEntity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesEntryEntity other = (SalesEntryEntity) obj;
		if (productEntity == null) {
			if (other.productEntity != null)
				return false;
		} else if (!productEntity.equals(other.productEntity))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (salesOrderEntity == null) {
			if (other.salesOrderEntity != null)
				return false;
		} else if (!salesOrderEntity.equals(other.salesOrderEntity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesEntryEntity [quantity=" + quantity + ", salesOrderEntity="
				+ salesOrderEntity + ", productEntity=" + productEntity + "]";
	}

}
