package id.bts.spartacommerce.domains;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class AdjustmentEntryEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "adjustment_id", nullable = false)
	private AdjustmentEntity adjustmentEntity;

	@OneToOne
	@JoinColumn(name = "product_id", nullable = false)
	private ProductEntity productEntity;

	private Integer quantity;

	public AdjustmentEntity getAdjustmentEntity() {
		return adjustmentEntity;
	}

	public void setAdjustmentEntity(AdjustmentEntity adjustmentEntity) {
		this.adjustmentEntity = adjustmentEntity;
	}

	public ProductEntity getProductEntity() {
		return productEntity;
	}

	public void setProductEntity(ProductEntity productEntity) {
		this.productEntity = productEntity;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((adjustmentEntity == null) ? 0 : adjustmentEntity.hashCode());
		result = prime * result + ((productEntity == null) ? 0 : productEntity.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdjustmentEntryEntity other = (AdjustmentEntryEntity) obj;
		if (adjustmentEntity == null) {
			if (other.adjustmentEntity != null)
				return false;
		} else if (!adjustmentEntity.equals(other.adjustmentEntity))
			return false;
		if (productEntity == null) {
			if (other.productEntity != null)
				return false;
		} else if (!productEntity.equals(other.productEntity))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " AdjustmentEntryEntity [adjustmentEntity=" + adjustmentEntity + ", productEntity="
				+ productEntity + ", quantity=" + quantity + "]";
	}

}
