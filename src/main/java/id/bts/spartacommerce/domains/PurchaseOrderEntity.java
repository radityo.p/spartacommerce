package id.bts.spartacommerce.domains;

import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class PurchaseOrderEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	private ZonedDateTime date;
	
	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private UserEntity userEntity;
	
	@OneToOne
	@JoinColumn(name = "vendor_id", nullable = false)
	private VendorEntity vendorEntity;

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public VendorEntity getVendorEntity() {
		return vendorEntity;
	}

	public void setVendorEntity(VendorEntity vendorEntity) {
		this.vendorEntity = vendorEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((userEntity == null) ? 0 : userEntity.hashCode());
		result = prime * result + ((vendorEntity == null) ? 0 : vendorEntity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrderEntity other = (PurchaseOrderEntity) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (userEntity == null) {
			if (other.userEntity != null)
				return false;
		} else if (!userEntity.equals(other.userEntity))
			return false;
		if (vendorEntity == null) {
			if (other.vendorEntity != null)
				return false;
		} else if (!vendorEntity.equals(other.vendorEntity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " PurchaseOrderEntity [date=" + date + ", userEntity=" + userEntity
				+ ", vendorEntity=" + vendorEntity + "]";
	}
	
	
	
}
