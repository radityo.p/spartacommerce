package id.bts.spartacommerce.domains;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class PurchaseEntryEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private Integer quantity;

	@OneToOne
	@JoinColumn(name = "product_id", nullable = false)
	private ProductEntity productEntity;
	
	@ManyToOne
	@JoinColumn(name = "purchaseOrder_id", nullable = false)
	private PurchaseOrderEntity purchaseOrderEntity;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ProductEntity getProductEntity() {
		return productEntity;
	}

	public void setProductEntity(ProductEntity productEntity) {
		this.productEntity = productEntity;
	}

	public PurchaseOrderEntity getPurchaseOrderEntity() {
		return purchaseOrderEntity;
	}

	public void setPurchaseOrderEntity(PurchaseOrderEntity purchaseOrderEntity) {
		this.purchaseOrderEntity = purchaseOrderEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((productEntity == null) ? 0 : productEntity.hashCode());
		result = prime * result + ((purchaseOrderEntity == null) ? 0 : purchaseOrderEntity.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseEntryEntity other = (PurchaseEntryEntity) obj;
		if (productEntity == null) {
			if (other.productEntity != null)
				return false;
		} else if (!productEntity.equals(other.productEntity))
			return false;
		if (purchaseOrderEntity == null) {
			if (other.purchaseOrderEntity != null)
				return false;
		} else if (!purchaseOrderEntity.equals(other.purchaseOrderEntity))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " PurchaseEntryEntity [quantity=" + quantity + ", productEntity="
				+ productEntity + ", purchaseOrderEntity=" + purchaseOrderEntity + "]";
	}

}
