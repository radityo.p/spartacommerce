package id.bts.spartacommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpartaEcommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpartaEcommerceApplication.class, args);
	}

}
