package id.bts.spartacommerce.bootstrap;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import id.bts.spartacommerce.domains.AdjustmentEntity;
import id.bts.spartacommerce.domains.AdjustmentEntryEntity;
import id.bts.spartacommerce.domains.CategoryEntity;
import id.bts.spartacommerce.domains.ProductEntity;
import id.bts.spartacommerce.domains.PurchaseEntryEntity;
import id.bts.spartacommerce.domains.PurchaseOrderEntity;
import id.bts.spartacommerce.domains.SalesEntryEntity;
import id.bts.spartacommerce.domains.SalesOrderEntity;
import id.bts.spartacommerce.domains.UserEntity;
import id.bts.spartacommerce.domains.VendorEntity;
import id.bts.spartacommerce.repositories.AdjustmentEntryRepository;
import id.bts.spartacommerce.repositories.AdjustmentRepository;
import id.bts.spartacommerce.repositories.CategoryRepository;
import id.bts.spartacommerce.repositories.ProductRepository;
import id.bts.spartacommerce.repositories.PurchaseEntryRepository;
import id.bts.spartacommerce.repositories.PurchaseOrderRepository;
import id.bts.spartacommerce.repositories.SalesEntryRepository;
import id.bts.spartacommerce.repositories.SalesOrderRepository;
import id.bts.spartacommerce.repositories.UserRepository;
import id.bts.spartacommerce.repositories.VendorRepository;

@Component
public class InitData implements CommandLineRunner {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AdjustmentRepository adjustmentRepository;

	@Autowired
	private AdjustmentEntryRepository adjustmentEntryRepository;

	@Autowired
	private SalesOrderRepository salesOrderRepository;

	@Autowired
	private SalesEntryRepository salesEntryRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private PurchaseOrderRepository purchaseOrderRepository;

	@Autowired
	private PurchaseEntryRepository purchaseEntryRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void run(String... args) throws Exception {
		if (categoryRepository.findAll().isEmpty())
			initData();
	}

	private void initData() {

		List<CategoryEntity> categories = new ArrayList<>();

		CategoryEntity category1 = new CategoryEntity();
		category1.setId(1L);
		category1.setName("Fresh Food");
		categories.add(category1);

		CategoryEntity category2 = new CategoryEntity();
		category2.setId(2L);
		category2.setName("Processed Food");
		categories.add(category2);

		categoryRepository.saveAll(categories);

		ProductEntity product1 = new ProductEntity();
		product1.setName("Udang Kupas");
		product1.setPrice(5000);
		product1.setQuantity(750);
		product1.setDescription("Udang yang ga ada kulit dan kepalanya");
		product1.setCategory(category1);

		product1 = productRepository.save(product1);

		ProductEntity product2 = new ProductEntity();
		product2.setName("Mie Tiga Roda");
		product2.setPrice(2000);
		product2.setQuantity(10000);
		product2.setDescription("Mie kemasan seperti pada umumnya aja");
		product2.setCategory(category2);

		product2 = productRepository.save(product2);

		UserEntity user1 = new UserEntity();
		user1.setName("nama User1");
		user1.setPassword(passwordEncoder.encode("123456789"));
		user1.setRole("CUSTOMER");
		user1.setEmail("awawaw@wawa.com");
		user1 = userRepository.save(user1);

		AdjustmentEntity adjustment1 = new AdjustmentEntity();
		adjustment1.setDate(ZonedDateTime.now());
		adjustment1.setUserEntity(user1);

		adjustment1 = adjustmentRepository.save(adjustment1);

		AdjustmentEntryEntity adjustmentEntry1 = new AdjustmentEntryEntity();
		adjustmentEntry1.setProductEntity(product1);
		adjustmentEntry1.setQuantity(9);
		adjustmentEntry1.setAdjustmentEntity(adjustment1);

		adjustmentEntryRepository.save(adjustmentEntry1);

		SalesOrderEntity salesOrder1 = new SalesOrderEntity();
		salesOrder1.setDate(ZonedDateTime.now());
		salesOrder1.setUserEntity(user1);
		salesOrder1 = salesOrderRepository.save(salesOrder1);

		SalesEntryEntity salesEntry1 = new SalesEntryEntity();
		salesEntry1.setProductEntity(product1);
		salesEntry1.setQuantity(4);
		salesEntry1.setSalesOrderEntity(salesOrder1);
		salesEntry1 = salesEntryRepository.save(salesEntry1);

		VendorEntity vendor1 = new VendorEntity();
		vendor1.setName("PT. Maju Jaya");
		vendor1.setAddress("Sebrang jalan indomaret cipto, Cirebon");
		vendor1 = vendorRepository.save(vendor1);

		PurchaseOrderEntity purchaseOrder1 = new PurchaseOrderEntity();
		purchaseOrder1.setDate(ZonedDateTime.now());
		purchaseOrder1.setUserEntity(user1);
		purchaseOrder1.setVendorEntity(vendor1);
		purchaseOrder1 = purchaseOrderRepository.save(purchaseOrder1);

		PurchaseEntryEntity purchaseEntry1 = new PurchaseEntryEntity();
		purchaseEntry1.setProductEntity(product1);
		purchaseEntry1.setPurchaseOrderEntity(purchaseOrder1);
		purchaseEntry1.setQuantity(10);
		purchaseEntry1 = purchaseEntryRepository.save(purchaseEntry1);

	}

}
