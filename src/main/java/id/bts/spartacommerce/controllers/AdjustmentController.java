package id.bts.spartacommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.bts.spartacommerce.DTOs.request.AdjustmentPostRequest;
import id.bts.spartacommerce.DTOs.response.AdjustmentDTO;
import id.bts.spartacommerce.services.AdjustmentService;

@RestController
@RequestMapping(path = "/api/v1/adjustments")
public class AdjustmentController {

	@Autowired
	private AdjustmentService adjustmentService;
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<AdjustmentDTO> getAllAdjustments() {
		return adjustmentService.getAllAdjustments();
	}
	
	@GetMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public AdjustmentDTO getAdjustmentById(@PathVariable(name = "id") Long id) {
		
		return adjustmentService.getAdjustmentById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public AdjustmentDTO createAdjustment(@RequestBody AdjustmentPostRequest adjustmentPostRequest) {

		return adjustmentService.createAdjustment(adjustmentPostRequest);
	}
}
