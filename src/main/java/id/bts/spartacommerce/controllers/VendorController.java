package id.bts.spartacommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.bts.spartacommerce.domains.VendorEntity;
import id.bts.spartacommerce.services.VendorService;

@RestController
@RequestMapping(path = "/api/v1/vendors")
public class VendorController {

	@Autowired
	private VendorService vendorService;

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<VendorEntity> getAllVendors() {
		return vendorService.getAllvendor();
	}

	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public VendorEntity getVendorById(@PathVariable(name = "id") Long id) {
		return vendorService.getVendorById(id);
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public VendorEntity createVendor(@RequestBody VendorEntity vendorEntity) {
		return vendorService.createVendor(vendorEntity);
	}

	@PutMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public VendorEntity updateVendor(@RequestBody VendorEntity vendorEntity, @PathVariable Long id) {
		return vendorService.updateVendor(vendorEntity, id);
	}

	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteVendorById(@PathVariable(name = "id") Long id) {
		vendorService.deleteVendor(id);
	}

}
