package id.bts.spartacommerce.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bts.spartacommerce.DTOs.request.LoginPostRequest;
import id.bts.spartacommerce.DTOs.response.JwtTokenPayload;
import id.bts.spartacommerce.security.JwtTokenUtil;
import id.bts.spartacommerce.security.JwtUser;

@RestController
@RequestMapping(path = "/api/v1")
public class AuthController {

	
	private static final Logger log = LoggerFactory.getLogger(AuthController.class);

	
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@PostMapping(path = "/login")
	public JwtTokenPayload login(@RequestBody LoginPostRequest loginPostRequest) {
		
		UserDetails userDetails = (JwtUser) userDetailsService.loadUserByUsername(loginPostRequest.getUsername());
		boolean isValid = passwordEncoder.matches(loginPostRequest.getPassword(), userDetails.getPassword());
		
		if (isValid) {
			
			JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			log.debug(user.toString());
			
			String token = jwtTokenUtil.generateToken((JwtUser) userDetails);
			JwtTokenPayload tokenPayload = new JwtTokenPayload();
			tokenPayload.setToken(token);
			return tokenPayload;
		} else {
			throw new UsernameNotFoundException("Either your Password or Username is wrong");
		}
	}
}
