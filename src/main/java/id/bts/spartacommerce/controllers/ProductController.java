package id.bts.spartacommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.bts.spartacommerce.DTOs.request.ProductPostRequest;
import id.bts.spartacommerce.DTOs.request.ProductPutRequest;
import id.bts.spartacommerce.DTOs.response.ProductDTO;
import id.bts.spartacommerce.domains.ProductArchiveEntity;
import id.bts.spartacommerce.services.ProductService;

@RestController
@RequestMapping("/api/v1//products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<ProductDTO> getAllProducts() {
		return productService.getAllProducts();
	}

	@GetMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public ProductDTO getProductById(@PathVariable Long id) {
		return productService.getProductById(id);
	}
	
	@GetMapping(path = "/archives")
	public List<ProductArchiveEntity> getAllProductArchive() {
		return productService.getAllArchivedProducts();
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public ProductDTO createNewProduct(@RequestBody ProductPostRequest productPostRequest) {
		return productService.saveProduct(productPostRequest);
	}
	
	@PutMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public ProductDTO updateProduct(@RequestBody ProductPutRequest productPutRequest, @PathVariable(name = "id") Long id) {
		
		return productService.updateProduct(productPutRequest, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void archiveProduct(@PathVariable(name = "id") Long id) {
		productService.archiveProduct(id);
	}
}
