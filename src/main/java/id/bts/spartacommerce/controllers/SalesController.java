package id.bts.spartacommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.bts.spartacommerce.DTOs.request.SalesOrderPostRequest;
import id.bts.spartacommerce.DTOs.response.SalesOrderDTO;
import id.bts.spartacommerce.services.SalesService;

@RestController
@RequestMapping("/api/v1/sales")
public class SalesController {

	@Autowired
	private SalesService salesService;
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<SalesOrderDTO> getAllSalesOrders() {
		return salesService.getAllSalesOrder();
	}
	
	@GetMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public SalesOrderDTO getSalesOrderById(@PathVariable(name = "id") Long id) {
		return salesService.getSalesOrderById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public SalesOrderDTO createSalesOrder(@RequestBody SalesOrderPostRequest salesOrderPostRequest) {
		return salesService.createSalesOrder(salesOrderPostRequest);
	}
}
