package id.bts.spartacommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.bts.spartacommerce.DTOs.request.PurchaseOrderPostRequest;
import id.bts.spartacommerce.DTOs.response.PurchaseOrderDTO;
import id.bts.spartacommerce.services.PurchaseService;

@RestController
@RequestMapping(path = "/api/v1/purchases")
public class PurchaseController {

	@Autowired
	private PurchaseService purchaseService;
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<PurchaseOrderDTO> getAllPurchaseOrder() {
		return purchaseService.getAllPurchaseOrder();
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public PurchaseOrderDTO getPurchaseOrderById(@PathVariable(name = "id") Long id) {
		return purchaseService.getPurchaseOrderById(id);
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public PurchaseOrderDTO createPurchaseOrder(@RequestBody PurchaseOrderPostRequest purchaseOrderPostRequest) {
		return purchaseService.createPurchaseOrder(purchaseOrderPostRequest);
	}
	
}
