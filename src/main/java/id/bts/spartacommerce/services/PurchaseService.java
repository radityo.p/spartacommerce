package id.bts.spartacommerce.services;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bts.spartacommerce.DTOs.request.PurchaseEntryPostRequest;
import id.bts.spartacommerce.DTOs.request.PurchaseOrderPostRequest;
import id.bts.spartacommerce.DTOs.response.PurchaseEntryDTO;
import id.bts.spartacommerce.DTOs.response.PurchaseOrderDTO;
import id.bts.spartacommerce.domains.ProductEntity;
import id.bts.spartacommerce.domains.PurchaseEntryEntity;
import id.bts.spartacommerce.domains.PurchaseOrderEntity;
import id.bts.spartacommerce.domains.UserEntity;
import id.bts.spartacommerce.repositories.ProductRepository;
import id.bts.spartacommerce.repositories.PurchaseEntryRepository;
import id.bts.spartacommerce.repositories.PurchaseOrderRepository;
import id.bts.spartacommerce.repositories.UserRepository;
import id.bts.spartacommerce.repositories.VendorRepository;
import id.bts.spartacommerce.security.JwtUser;

@Service
@Transactional
public class PurchaseService {

	@Autowired
	private PurchaseOrderRepository purchaseOrderRepository;
	@Autowired
	private PurchaseEntryRepository purchaseEntryRepository;
	@Autowired
	private VendorRepository vendorRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private UserRepository userRepository;

	@Transactional(readOnly = true)
	public List<PurchaseOrderDTO> getAllPurchaseOrder() {

		List<PurchaseOrderDTO> purchaseDTOs = new ArrayList<>();

		List<PurchaseOrderEntity> purchases = purchaseOrderRepository.findAll();
		for (PurchaseOrderEntity purchase : purchases) {

			PurchaseOrderDTO purchaseDTO = new PurchaseOrderDTO(purchase);
			List<PurchaseEntryEntity> entries = purchaseEntryRepository.findAll();

			for (PurchaseEntryEntity entry : entries) {
				PurchaseEntryDTO entryDTO = new PurchaseEntryDTO(entry);
				purchaseDTO.addPurchaseEntries(entryDTO);
			}

			purchaseDTOs.add(purchaseDTO);
		}

		return purchaseDTOs;
	}

	@Transactional(readOnly = true)
	public PurchaseOrderDTO getPurchaseOrderById(Long id) {
		
		PurchaseOrderEntity purchase = purchaseOrderRepository.findById(id).get();
		PurchaseOrderDTO purchaseDTO = new PurchaseOrderDTO(purchase);
		
		List<PurchaseEntryEntity> entries = purchaseEntryRepository.findByPurchaseOrderEntity_Id(id);
		
		for (PurchaseEntryEntity entry : entries) {
			PurchaseEntryDTO entryDTO = new PurchaseEntryDTO(entry);
			purchaseDTO.addPurchaseEntries(entryDTO);
		}
		
		return purchaseDTO;
	}
	
	public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderPostRequest purchaseOrderPostRequest) {
		PurchaseOrderEntity purchase = new PurchaseOrderEntity();
		purchase.setDate(ZonedDateTime.now());
		
		JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserEntity user = userRepository.findByEmailIgnoreCase(jwtUser.getUsername());
		
		purchase.setUserEntity(user);
		purchase.setVendorEntity(vendorRepository.findById(purchaseOrderPostRequest.getVendor_id()).get());
		
		purchase = purchaseOrderRepository.save(purchase);
		PurchaseOrderDTO purchaseDTO = new PurchaseOrderDTO(purchase);
		for (PurchaseEntryPostRequest entryPost : purchaseOrderPostRequest.getEntries()) {
			PurchaseEntryEntity entry = new PurchaseEntryEntity();
			entry.setPurchaseOrderEntity(purchase);
			entry.setQuantity(entryPost.getQuantity());
			
			ProductEntity product = productRepository.findById(entryPost.getProduct_id()).get();
			product.setQuantity(product.getQuantity() + entry.getQuantity());
			entry.setProductEntity(productRepository.save(product));
			
			purchaseDTO.addPurchaseEntries(new PurchaseEntryDTO(purchaseEntryRepository.save(entry)));
		}
		
		return purchaseDTO;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
