package id.bts.spartacommerce.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bts.spartacommerce.domains.VendorEntity;
import id.bts.spartacommerce.repositories.VendorRepository;

@Service
@Transactional
public class VendorService {

	@Autowired
	private VendorRepository vendorRepository;

	public List<VendorEntity> getAllvendor () {
		return vendorRepository.findAll();
	}
	
	public VendorEntity getVendorById(Long id) {
		return vendorRepository.findById(id).get();
	}
	
	public VendorEntity createVendor(VendorEntity vendorEntity) {
		vendorEntity.setId(null);
		return vendorRepository.save(vendorEntity);
	}
	
	public VendorEntity updateVendor(VendorEntity vendorEntity, Long id) {
		VendorEntity vendor = vendorRepository.findById(id).get();
		vendor.setAddress(vendorEntity.getAddress());
		vendor.setName(vendorEntity.getName());
		
		return vendorRepository.save(vendor);
	}
	
	public void deleteVendor(Long id) {
		vendorRepository.deleteById(id);
	}
}
