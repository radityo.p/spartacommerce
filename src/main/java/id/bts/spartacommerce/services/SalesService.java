package id.bts.spartacommerce.services;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bts.spartacommerce.DTOs.request.SalesEntryPostRequest;
import id.bts.spartacommerce.DTOs.request.SalesOrderPostRequest;
import id.bts.spartacommerce.DTOs.response.SalesEntryDTO;
import id.bts.spartacommerce.DTOs.response.SalesOrderDTO;
import id.bts.spartacommerce.domains.ProductEntity;
import id.bts.spartacommerce.domains.SalesEntryEntity;
import id.bts.spartacommerce.domains.SalesOrderEntity;
import id.bts.spartacommerce.domains.UserEntity;
import id.bts.spartacommerce.repositories.ProductRepository;
import id.bts.spartacommerce.repositories.SalesEntryRepository;
import id.bts.spartacommerce.repositories.SalesOrderRepository;
import id.bts.spartacommerce.repositories.UserRepository;
import id.bts.spartacommerce.security.JwtUser;

@Service
@Transactional
public class SalesService {

	
	private static final Logger log = LoggerFactory.getLogger(SalesService.class);

	@Autowired
	private SalesOrderRepository salesOrderRepository;
	@Autowired
	private SalesEntryRepository salesEntryRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private UserRepository userRepository;
	
	@Transactional(readOnly = true)
	public List<SalesOrderDTO> getAllSalesOrder() {
		List<SalesOrderEntity> salesOrders = salesOrderRepository.findAll();
		List<SalesOrderDTO> salesDTOs = new ArrayList<>();
		
		for (SalesOrderEntity salesOrder : salesOrders) {
			SalesOrderDTO salesDTO = new SalesOrderDTO(salesOrder);
			List<SalesEntryEntity> salesEntries = salesEntryRepository.findBySalesOrderEntity_Id(salesOrder.getId());
			
			for (SalesEntryEntity salesEntry : salesEntries) {
				salesDTO.addSalesEntry(new SalesEntryDTO(salesEntry));
			}
			
			salesDTOs.add(salesDTO);
		}
		
		return salesDTOs;
	}
	
	public SalesOrderDTO getSalesOrderById(Long id) {
		SalesOrderEntity salesOrder = salesOrderRepository.findById(id).get();
		List<SalesEntryEntity> salesEntries = salesEntryRepository.findBySalesOrderEntity_Id(id);
		SalesOrderDTO salesDTO = new SalesOrderDTO(salesOrder);
		
		for (SalesEntryEntity salesEntry : salesEntries) {
			salesDTO.addSalesEntry(new SalesEntryDTO(salesEntry));
		}
		
		return salesDTO;
	}
	
	public SalesOrderDTO createSalesOrder(SalesOrderPostRequest salesOrderPostRequest) {
		
		SalesOrderEntity salesOrder = new SalesOrderEntity();
		salesOrder.setDate(ZonedDateTime.now());
		
		JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserEntity user = userRepository.findByEmailIgnoreCase(jwtUser.getUsername());
		
		salesOrder.setUserEntity(user);
		salesOrder = salesOrderRepository.save(salesOrder);
		
		SalesOrderDTO salesOrderDTO = new SalesOrderDTO(salesOrder);
		log.debug(salesOrderPostRequest.toString());
		for (SalesEntryPostRequest entryPost : salesOrderPostRequest.getSalesEntries()) {
			
			SalesEntryEntity entry = new SalesEntryEntity();
			entry.setQuantity(entryPost.getQuantity());
			entry.setSalesOrderEntity(salesOrder);
			
			ProductEntity product = productRepository.findById(entryPost.getProduct_id()).get();
			product.setQuantity(product.getQuantity() - entryPost.getQuantity());
			
			entry.setProductEntity(productRepository.save(product));
			
			salesOrderDTO.addSalesEntry(new SalesEntryDTO(salesEntryRepository.save(entry)));
		}
		return salesOrderDTO;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
