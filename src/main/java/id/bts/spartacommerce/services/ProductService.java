package id.bts.spartacommerce.services;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bts.spartacommerce.DTOs.request.ProductPostRequest;
import id.bts.spartacommerce.DTOs.request.ProductPutRequest;
import id.bts.spartacommerce.DTOs.response.ProductDTO;
import id.bts.spartacommerce.domains.ProductArchiveEntity;
import id.bts.spartacommerce.domains.ProductEntity;
import id.bts.spartacommerce.domains.UserEntity;
import id.bts.spartacommerce.repositories.CategoryRepository;
import id.bts.spartacommerce.repositories.ProductArchiveRepository;
import id.bts.spartacommerce.repositories.ProductRepository;
import id.bts.spartacommerce.repositories.UserRepository;
import id.bts.spartacommerce.security.JwtUser;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProductArchiveRepository productArchiveRepository;

	@Autowired
	private UserRepository userRepository;

	@Transactional(readOnly = true)
	public List<ProductDTO> getAllProducts() {
		List<ProductDTO> productDTOs = new ArrayList<>();

		List<ProductEntity> products = productRepository.findAll();

		for (ProductEntity product : products) {
			productDTOs.add(new ProductDTO(product));
		}
		return productDTOs;
	}

	@Transactional(readOnly = true)
	public ProductDTO getProductById(Long id) {
		return new ProductDTO(productRepository.findById(id).get());
	}

	@Transactional(readOnly = true)
	public List<ProductArchiveEntity> getAllArchivedProducts() {
		return productArchiveRepository.findAll();
	}

	public ProductDTO saveProduct(ProductPostRequest productPostRequest) {
		ProductEntity product = new ProductEntity();
		product.setName(productPostRequest.getName());
		product.setPrice(productPostRequest.getPrice());
		product.setQuantity(productPostRequest.getQuantity());
		product.setDescription(productPostRequest.getDescription());
		product.setCategory(categoryRepository.findById(productPostRequest.getCategory_id()).get());
		product = productRepository.save(product);
		ProductDTO productDTO = new ProductDTO(product);

		return productDTO;
	}

	public ProductDTO updateProduct(ProductPutRequest productPutRequest, Long id) {
		ProductEntity product = productRepository.findById(id).get();
		product.setName(productPutRequest.getName());
		product.setPrice(productPutRequest.getPrice());
		product.setDescription(productPutRequest.getDescription());
		product.setCategory(categoryRepository.findById(productPutRequest.getCategory_id()).get());
		product = productRepository.save(product);
		ProductDTO productDTO = new ProductDTO(product);

		return productDTO;

	}

	public void archiveProduct(Long id) {
		ProductEntity product = productRepository.findById(id).get();

		ProductArchiveEntity productArchive = new ProductArchiveEntity();
		productArchive.setDate(ZonedDateTime.now());
		productArchive.setName(product.getName());
		productArchive.setPrice(product.getPrice());
		productArchive.setDescription(product.getDescription());

		JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserEntity user = userRepository.findByEmailIgnoreCase(jwtUser.getUsername());
		
		productArchive.setUserEntity(user);

		productArchiveRepository.save(productArchive);

		productRepository.delete(product);

	}

}
