package id.bts.spartacommerce.services;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bts.spartacommerce.DTOs.request.AdjustmentEntryPostRequest;
import id.bts.spartacommerce.DTOs.request.AdjustmentPostRequest;
import id.bts.spartacommerce.DTOs.response.AdjustmentDTO;
import id.bts.spartacommerce.DTOs.response.AdjustmentEntryDTO;
import id.bts.spartacommerce.domains.AdjustmentEntity;
import id.bts.spartacommerce.domains.AdjustmentEntryEntity;
import id.bts.spartacommerce.domains.ProductEntity;
import id.bts.spartacommerce.domains.UserEntity;
import id.bts.spartacommerce.repositories.AdjustmentEntryRepository;
import id.bts.spartacommerce.repositories.AdjustmentRepository;
import id.bts.spartacommerce.repositories.ProductRepository;
import id.bts.spartacommerce.repositories.UserRepository;
import id.bts.spartacommerce.security.JwtUser;

@Service
@Transactional
public class AdjustmentService {

	@Autowired
	private AdjustmentRepository adjustmentRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private AdjustmentEntryRepository adjustmentEntryRepository;
	@Autowired
	private UserRepository userRepository;

	@Transactional(readOnly = true)
	public List<AdjustmentDTO> getAllAdjustments() {

		List<AdjustmentEntity> adjustments = adjustmentRepository.findAll();

		List<AdjustmentDTO> adjustmentDTO = new ArrayList<>();

		for (AdjustmentEntity adjustment : adjustments) {
			AdjustmentDTO adjustDTO = new AdjustmentDTO(adjustment);

			List<AdjustmentEntryEntity> entries = adjustmentEntryRepository
					.findByAdjustmentEntity_Id(adjustment.getId());

			for (AdjustmentEntryEntity entry : entries) {
				adjustDTO.addEntries(new AdjustmentEntryDTO(entry));
			}

			adjustmentDTO.add(adjustDTO);
		}
		return adjustmentDTO;
	}
	
	@Transactional(readOnly = true)
	public AdjustmentDTO getAdjustmentById(Long id) {
		
		AdjustmentDTO adjustmentDTO = new AdjustmentDTO(adjustmentRepository.findById(id).get());
		List<AdjustmentEntryEntity> entries = adjustmentEntryRepository.findByAdjustmentEntity_Id(adjustmentDTO.getId());
		
		for (AdjustmentEntryEntity entry : entries) {
			adjustmentDTO.addEntries(new AdjustmentEntryDTO(entry));
		}
		
		return adjustmentDTO;
	}
	
	public AdjustmentDTO createAdjustment(AdjustmentPostRequest adjustmentPostRequest) {
		
		AdjustmentEntity adjustment = new AdjustmentEntity();
		adjustment.setDate(ZonedDateTime.of(
				LocalDateTime.ofEpochSecond(adjustmentPostRequest.getDate() * 1000, 0, ZoneOffset.UTC),
				ZoneId.systemDefault()));
		
		JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		UserEntity user = userRepository.findByEmailIgnoreCase(jwtUser.getUsername());
		
		adjustment.setUserEntity(user);
		
		adjustment = adjustmentRepository.save(adjustment);
		
		AdjustmentDTO adjustmentDTO = new AdjustmentDTO(adjustment);
		
		for(AdjustmentEntryPostRequest entryPost : adjustmentPostRequest.getEntries()) {
			
			AdjustmentEntryEntity entry = new AdjustmentEntryEntity();
			entry.setAdjustmentEntity(adjustment);
			
			ProductEntity product = productRepository.findById(entryPost.getProduct_id()).get();
			
			Integer initialQuantity = product.getQuantity();
			Integer finalQuantity = initialQuantity + entryPost.getQuantity();
			
			product.setQuantity(finalQuantity);
			
			entry.setProductEntity(product);
			entry.setQuantity(entryPost.getQuantity());
			
			adjustmentDTO.addEntries(new AdjustmentEntryDTO(adjustmentEntryRepository.save(entry)));
		}
		
		return adjustmentDTO;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
