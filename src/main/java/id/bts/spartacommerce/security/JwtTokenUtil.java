package id.bts.spartacommerce.security;

import java.time.ZonedDateTime;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil {

	
	private static final Logger log = LoggerFactory.getLogger(JwtTokenUtil.class);

	private final String SECRET = "Sementara Symetryc key dulu";
	private final ZonedDateTime EXPIRATION = ZonedDateTime.now().plusMonths(1);

	public String generateToken(JwtUser userDetails) {
		return Jwts.builder().setSubject(userDetails.getUsername()).setExpiration(Date.from(EXPIRATION.toInstant()))
				.signWith(SignatureAlgorithm.HS512, SECRET).compact();
	}
	
	public boolean validateTokenExpiration(Claims claims) {
		final Date expiration = claims.getExpiration();
		return expiration.after(new Date());
	}
	
	public Claims getClaimsFromToken(String token) {
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
			log.debug("claims extracted from token: " + claims.toString());
		} catch (Exception e) {
			log.error("Token is not valid, exception: " + e.toString());
		}
		
		return claims;
	}

}
