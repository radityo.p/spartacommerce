package id.bts.spartacommerce.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.bts.spartacommerce.domains.UserEntity;
import id.bts.spartacommerce.repositories.UserRepository;

@Service
public class JwtUserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userRepository.findByEmailIgnoreCase(username);

		return new JwtUser(user.getPassword(), user.getEmail(), createGrantedAuthorities(user.getRole()));
	}

	private List<GrantedAuthority> createGrantedAuthorities(String authority) {

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_" + authority));

		return authorities;
	}

}
