package id.bts.spartacommerce.DTOs.request;

import java.util.List;

public class AdjustmentPostRequest {

	private Long date;
	private List<AdjustmentEntryPostRequest> entries;

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public List<AdjustmentEntryPostRequest> getEntries() {
		return entries;
	}

	public void setEntries(List<AdjustmentEntryPostRequest> entries) {
		this.entries = entries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((entries == null) ? 0 : entries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdjustmentPostRequest other = (AdjustmentPostRequest) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (entries == null) {
			if (other.entries != null)
				return false;
		} else if (!entries.equals(other.entries))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AdjustmentPostRequest [date=" + date + ", entries=" + entries + "]";
	}

}
