package id.bts.spartacommerce.DTOs.request;

import java.util.List;

public class SalesOrderPostRequest {

	private Long date;
	private List<SalesEntryPostRequest> salesEntries;

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public List<SalesEntryPostRequest> getSalesEntries() {
		return salesEntries;
	}

	public void setSalesEntries(List<SalesEntryPostRequest> salesEntries) {
		this.salesEntries = salesEntries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((salesEntries == null) ? 0 : salesEntries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesOrderPostRequest other = (SalesOrderPostRequest) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (salesEntries == null) {
			if (other.salesEntries != null)
				return false;
		} else if (!salesEntries.equals(other.salesEntries))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesOrderPostRequest [date=" + date + ", salesEntries=" + salesEntries + "]";
	}

}
