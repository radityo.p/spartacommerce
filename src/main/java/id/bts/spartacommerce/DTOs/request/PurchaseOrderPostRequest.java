package id.bts.spartacommerce.DTOs.request;

import java.util.List;

public class PurchaseOrderPostRequest {

	private Long vendor_id;
	private List<PurchaseEntryPostRequest> entries;

	public Long getVendor_id() {
		return vendor_id;
	}

	public void setVendor_id(Long vendor_id) {
		this.vendor_id = vendor_id;
	}

	public List<PurchaseEntryPostRequest> getEntries() {
		return entries;
	}

	public void setEntries(List<PurchaseEntryPostRequest> entries) {
		this.entries = entries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entries == null) ? 0 : entries.hashCode());
		result = prime * result + ((vendor_id == null) ? 0 : vendor_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrderPostRequest other = (PurchaseOrderPostRequest) obj;
		if (entries == null) {
			if (other.entries != null)
				return false;
		} else if (!entries.equals(other.entries))
			return false;
		if (vendor_id == null) {
			if (other.vendor_id != null)
				return false;
		} else if (!vendor_id.equals(other.vendor_id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PurchaseOrderPostRequest [vendor_id=" + vendor_id + ", entries=" + entries + "]";
	}

}
