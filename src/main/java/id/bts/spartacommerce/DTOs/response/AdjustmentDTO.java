package id.bts.spartacommerce.DTOs.response;

import java.util.ArrayList;
import java.util.List;

import id.bts.spartacommerce.domains.AdjustmentEntity;

public class AdjustmentDTO {

	private Long id;
	private UserDTO userDTO;
	private Long date;
	private List<AdjustmentEntryDTO> entries;

	public AdjustmentDTO() {
		super();
		this.entries = new ArrayList<>();
	}

	public AdjustmentDTO(AdjustmentEntity adjustment) {
		super();
		this.id = adjustment.getId();
		this.userDTO = new UserDTO(adjustment.getUserEntity());
		this.date = adjustment.getDate().toEpochSecond() * 1000;
		this.entries = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public List<AdjustmentEntryDTO> getEntries() {
		return entries;
	}

	public void setEntries(List<AdjustmentEntryDTO> entries) {
		this.entries = entries;
	}
	
	public void addEntries(AdjustmentEntryDTO entries) {
		this.entries.add(entries);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((entries == null) ? 0 : entries.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((userDTO == null) ? 0 : userDTO.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdjustmentDTO other = (AdjustmentDTO) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (entries == null) {
			if (other.entries != null)
				return false;
		} else if (!entries.equals(other.entries))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (userDTO == null) {
			if (other.userDTO != null)
				return false;
		} else if (!userDTO.equals(other.userDTO))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AdjustmentDTO [id=" + id + ", userDTO=" + userDTO + ", date=" + date + ", entries=" + entries + "]";
	}

}
