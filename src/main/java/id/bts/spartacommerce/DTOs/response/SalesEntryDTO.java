package id.bts.spartacommerce.DTOs.response;

import id.bts.spartacommerce.domains.SalesEntryEntity;

public class SalesEntryDTO {

	private Long id;
	private Integer quantity;
	private ProductDTO product;

	public SalesEntryDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SalesEntryDTO(SalesEntryEntity salesEntry) {
		super();
		this.id = salesEntry.getId();
		this.quantity = salesEntry.getQuantity();
		this.product = new ProductDTO(salesEntry.getProductEntity());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ProductDTO getProduct() {
		return product;
	}

	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesEntryDTO other = (SalesEntryDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesEntryDTO [id=" + id + ", quantity=" + quantity + ", product=" + product + "]";
	}
}
