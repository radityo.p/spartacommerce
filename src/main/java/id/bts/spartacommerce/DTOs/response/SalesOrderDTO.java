package id.bts.spartacommerce.DTOs.response;

import java.util.ArrayList;
import java.util.List;

import id.bts.spartacommerce.domains.SalesOrderEntity;

public class SalesOrderDTO {

	private Long id;
	private Long date;
	private UserDTO user;
	private List<SalesEntryDTO> salesEntries;

	public SalesOrderDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SalesOrderDTO(SalesOrderEntity salesOrder) {
		super();
		this.id = salesOrder.getId();
		this.date = salesOrder.getDate().toEpochSecond();
		this.user = new UserDTO(salesOrder.getUserEntity());
		this.salesEntries = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public List<SalesEntryDTO> getSalesEntries() {
		return salesEntries;
	}

	public void setSalesEntries(List<SalesEntryDTO> salesEntries) {
		this.salesEntries = salesEntries;
	}
	
	public void addSalesEntry(SalesEntryDTO salesEntry) {
		this.salesEntries.add(salesEntry);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((salesEntries == null) ? 0 : salesEntries.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesOrderDTO other = (SalesOrderDTO) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (salesEntries == null) {
			if (other.salesEntries != null)
				return false;
		} else if (!salesEntries.equals(other.salesEntries))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SalesOrderDTO [id=" + id + ", date=" + date + ", user=" + user + ", salesEntries=" + salesEntries + "]";
	}
}
