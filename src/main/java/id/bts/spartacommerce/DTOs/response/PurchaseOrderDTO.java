package id.bts.spartacommerce.DTOs.response;

import java.util.ArrayList;
import java.util.List;

import id.bts.spartacommerce.domains.PurchaseOrderEntity;

public class PurchaseOrderDTO {

	private Long id;
	private Long date;
	private UserDTO user;
	private VendorDTO vendor;
	private List<PurchaseEntryDTO> purchaseEntries;

	public PurchaseOrderDTO() {
		super();
		this.purchaseEntries = new ArrayList<>();
	}

	public PurchaseOrderDTO(PurchaseOrderEntity purchaseOrder) {
		super();
		this.id = purchaseOrder.getId();
		this.date = purchaseOrder.getDate().toEpochSecond() * 1000;
		this.user = new UserDTO(purchaseOrder.getUserEntity());
		this.vendor = new VendorDTO(purchaseOrder.getVendorEntity());
		this.purchaseEntries = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public VendorDTO getVendor() {
		return vendor;
	}

	public void setVendor(VendorDTO vendor) {
		this.vendor = vendor;
	}

	public List<PurchaseEntryDTO> getPurchaseEntries() {
		return purchaseEntries;
	}

	public void setPurchaseEntries(List<PurchaseEntryDTO> purchaseEntries) {
		this.purchaseEntries = purchaseEntries;
	}
	
	public void addPurchaseEntries(PurchaseEntryDTO purchaseEntry) {
		this.purchaseEntries.add(purchaseEntry);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((purchaseEntries == null) ? 0 : purchaseEntries.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrderDTO other = (PurchaseOrderDTO) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (purchaseEntries == null) {
			if (other.purchaseEntries != null)
				return false;
		} else if (!purchaseEntries.equals(other.purchaseEntries))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (vendor == null) {
			if (other.vendor != null)
				return false;
		} else if (!vendor.equals(other.vendor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PurchaseOrderDTO [id=" + id + ", date=" + date + ", user=" + user + ", vendor=" + vendor
				+ ", purchaseEntries=" + purchaseEntries + "]";
	}

}
